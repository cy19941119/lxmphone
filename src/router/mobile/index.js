import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '../../components/mobile/HelloWorld'
import author from '../../components/mobile/author'
import eventDiscoveryMain from '../../components/mobile/eventDiscovery/main'
import manualReport from '../../components/mobile/eventDiscovery/manualReport'
import manualReportDone from '../../components/mobile/eventDiscovery/manualReportDone'
import manualReportDetail from '../../components/mobile/eventDiscovery/manualReportDetail'
import eventAIlist from '../../components/mobile/eventDiscovery/eventAIlist'
import AIReport from '../../components/mobile/eventDiscovery/AIReport'
import AIInforconfirm from '../../components/mobile/eventDiscovery/AIInforconfirm'
import AIReportDetail from '../../components/mobile/eventDiscovery/AIReportDetail'
import AIReportDone from '../../components/mobile/eventDiscovery/AIReportDone'
import errAIevent from '../../components/mobile/eventDiscovery/errAIevent'
import workingEventsMain from '../../components/mobile/workingEvents/main'
import garbageClassification from '../../components/mobile/workingEvents/garbageClassification'
import garbageDumped from '../../components/mobile/workingEvents/garbageDumped'
import classificationAiReport from '../../components/mobile/workingEvents/classification/classificationAiReport'
import classificationAiDetail from '../../components/mobile/workingEvents/classification/classificationAiDetail'
import classificationAiCase from '../../components/mobile/workingEvents/classification/classificationAiCase'
import classificationPeoReport from '../../components/mobile/workingEvents/classification/classificationPeoReport'
import classificationPeoDetail from '../../components/mobile/workingEvents/classification/classificationPeoDetail'
import classificationPeoCase from '../../components/mobile/workingEvents/classification/classificationPeoCase'
import dumpedAiReport from '../../components/mobile/workingEvents/dumped/dumpedAiReport'
import dumpedAiDetail from '../../components/mobile/workingEvents/dumped/dumpedAiDetail'
import dumpedAiCase from '../../components/mobile/workingEvents/dumped/dumpedAiCase'
import dumpedProReport from '../../components/mobile/workingEvents/dumped/dumpedProReport'
import dumpedProDetail from '../../components/mobile/workingEvents/dumped/dumpedProDetail'
import dumpedProCase from '../../components/mobile/workingEvents/dumped/dumpedProCase'
import eventsToPersonAi from '../../components/mobile/workingEvents/dumped/eventsToPersonAi'
import eventsToPersonAiDetail from '../../components/mobile/workingEvents/dumped/eventsToPersonAiDetail'
import eventsToPersonPeo from '../../components/mobile/workingEvents/dumped/eventsToPersonPeo'
import eventsToPersonPeoDetail from '../../components/mobile/workingEvents/dumped/eventsToPersonPeoDetail'

import regulatoryPower from '../../components/mobile/regulatoryPower/main'
import permSetting from '../../components/mobile/regulatoryPower/permSetting'
import powerAdd from '../../components/mobile/regulatoryPower/powerAdd'

import eventListMain from '../../components/mobile/eventList/main'
import eventListAiReportDetail from '../../components/mobile/eventList/eventListAiReportDetail'
import eventListPeoReportDetail from '../../components/mobile/eventList/eventListPeoReportDetail'

import chengguanEvents from '../../components/mobile/workingEvents/messageCard/chengguanEvents'
import chengguanCase from '../../components/mobile/workingEvents/messageCard/chengguanCase'
import chengguanDetail from '../../components/mobile/workingEvents/messageCard/chengguanDetail'

import gridEvents from '../../components/mobile/workingEvents/messageCard/gridEvents'
import gridCase from '../../components/mobile/workingEvents/messageCard/gridCase'
import gridDetail from '../../components/mobile/workingEvents/messageCard/gridDetail'

import trashPickupMain from '../../components/mobile/trashPickup/main'
import trashPickupCase from '../../components/mobile/trashPickup/trashPickupCase'

import AiReportCard from '../../components/mobile/eventDiscovery/AiReportCard'

Vue.use(Router)
let router = new Router({
  //  mode: 'history',  //去掉url中的#
  routes: [{
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
      meta: { requireAuth:true },
    },
    // {
    //   path: '/author',
    //   name: 'author',
    //   component: author,
    //   meta: { requireAuth:true },
    // },
    {
      path: '/eventDiscoveryMain',
      name: 'eventDiscoveryMain',
      component: eventDiscoveryMain,
    },
    {
      path: '/eventDiscoveryMain/manualReport',
      name: 'manualReport',
      component: manualReport,
    },
    {
      path: '/eventDiscoveryMain/manualReportDone',
      name: 'manualReportDone',
      component: manualReportDone,
    },
    {
      path: '/eventDiscoveryMain/manualReportDetail',
      name: 'manualReportDetail',
      component: manualReportDetail,
    },
    {
      path: '/eventDiscoveryMain/eventAIlist',
      name: 'eventAIlist',
      component: eventAIlist,
    },{
      path: '/eventDiscoveryMain/AIReport',
      name: 'AIReport',
      component: AIReport,
    },{
      path: '/eventDiscoveryMain/AIInforconfirm',
      name: 'AIInforconfirm',
      component: AIInforconfirm,
    },{
      path: '/eventDiscoveryMain/AIReportDetail',
      name: 'AIReportDetail',
      component: AIReportDetail,
    },{
      path: '/eventDiscoveryMain/AIReportDone',
      name: 'AIReportDone',
      component: AIReportDone,
    },{
      path: '/eventDiscoveryMain/errAIevent',
      name: 'errAIevent',
      component: errAIevent,
    },
    {
      path: '/workingEventsMain',
      name: 'workingEventsMain',
      component: workingEventsMain,
    },
    {
      path: '/workingEventsMain/garbageClassification',
      name: 'garbageClassification',
      component: garbageClassification,
    },
    {
      path: '/workingEventsMain/garbageDumped',
      name: 'garbageDumped',
      component: garbageDumped,
    },
    {
      path: '/workingEventsMain/classification/classificationAiReport',
      name: 'classificationAiReport',
      component: classificationAiReport,
    },
    {
      path: '/workingEventsMain/classification/classificationAiDetail',
      name: 'classificationAiDetail',
      component: classificationAiDetail,
    },
    {
      path: '/workingEventsMain/classification/classificationAiCase',
      name: 'classificationAiCase',
      component: classificationAiCase,
    },

    {
      path: '/workingEventsMain/classification/classificationPeoReport',
      name: 'classificationPeoReport',
      component: classificationPeoReport,
    },
    {
      path: '/workingEventsMain/classification/classificationPeoDetail',
      name: 'classificationPeoDetail',
      component: classificationPeoDetail,
    },
    {
      path: '/workingEventsMain/classification/classificationPeoCase',
      name: 'classificationPeoCase',
      component: classificationPeoCase,
    },

    {
      path: '/workingEventsMain/dumped/dumpedAiReport',
      name: 'dumpedAiReport',
      component: dumpedAiReport,
    },
    {
      path: '/workingEventsMain/dumped/dumpedAiDetail',
      name: 'dumpedAiDetail',
      component: dumpedAiDetail,
    },
    {
      path: '/workingEventsMain/dumped/dumpedAiCase',
      name: 'dumpedAiCase',
      component: dumpedAiCase,
    },
    {
      path: '/workingEventsMain/dumped/dumpedProReport',
      name: 'dumpedProReport',
      component: dumpedProReport,
    },
    {
      path: '/workingEventsMain/dumped/dumpedProDetail',
      name: 'dumpedProDetail',
      component: dumpedProDetail,
    },
    {
      path: '/workingEventsMain/dumped/dumpedProCase',
      name: 'dumpedProCase',
      component: dumpedProCase,
    },

    {
      path: '/workingEventsMain/dumped/eventsToPersonAi',
      name: 'eventsToPersonAi',
      component: eventsToPersonAi,
    },{
      path: '/workingEventsMain/dumped/eventsToPersonAiDetail',
      name: 'eventsToPersonAiDetail',
      component: eventsToPersonAiDetail,
    },{
      path: '/workingEventsMain/dumped/eventsToPersonPeo',
      name: 'eventsToPersonPeo',
      component: eventsToPersonPeo,
    },{
      path: '/workingEventsMain/dumped/eventsToPersonPeoDetail',
      name: 'eventsToPersonPeoDetail',
      component: eventsToPersonPeoDetail,
    },{
      path: '/eventListMain',
      name: 'eventListMain',
      component: eventListMain,
    },
    ,{
      path: '/eventListAiReportDetail',
      name: 'eventListAiReportDetail',
      component: eventListAiReportDetail,
    },,{
      path: '/eventListPeoReportDetail',
      name: 'eventListPeoReportDetail',
      component: eventListPeoReportDetail,
    },
    // {
    //   path: '/pages',
    //   name: 'page',
    //   component: root,
    //   // 添加该字段requireAuth: true,表示进入需要认证
    //   meta: {
    //     requireAuth: false
    //   },
    // }
    {
      path: '/regulatoryPower',
      name: 'regulatoryPower',
      component: regulatoryPower,
    },
    {
      path: '/regulatoryPower/permSetting',
      name: 'permSetting',
      component: permSetting,
    },{
      path: '/regulatoryPower/powerAdd',
      name: 'powerAdd',
      component: powerAdd,
    },

    {
      path: '/workingEventsMain/messageCard/chengguanEvents',
      name: 'chengguanEvents',
      component: chengguanEvents,
    },
    {
      path: '/workingEventsMain/messageCard/chengguanCase',
      name: 'chengguanCase',
      component: chengguanCase,
    },{
      path: '/workingEventsMain/messageCard/chengguanDetail',
      name: 'chengguanDetail',
      component: chengguanDetail,
    },
    {
      path: '/workingEventsMain/messageCard/gridEvents',
      name: 'gridEvents',
      component: gridEvents,
    },
    {
      path: '/workingEventsMain/messageCard/gridCase',
      name: 'gridCase',
      component: gridCase,
    },{
      path: '/workingEventsMain/messageCard/gridDetail',
      name: 'gridDetail',
      component: gridDetail,
    },
    {
      path: '/trashPickupMain',
      name: 'trashPickupMain',
      component: trashPickupMain,
    },
    {
      path: '/trashPickupMain/trashPickupCase',
      name: 'trashPickupCase',
      component: trashPickupCase,
    },
    {
      path: '/eventDiscoveryMain/AiReportCard',
      name: 'AiReportCard',
      component: AiReportCard,
    },
  ]
})

// router.beforeEach((to, from, next) => {
//   console.log('location='+location)
//   console.log('window.location='+sessionStorage.getItem('token'))
//   console.log('window.location.search='+window.location.search)
//   let str = window.location.href;
//   console.log('str='+str)
//   // if(!str){
//   if(str.indexOf("token=") == -1 ){
//     if(!sessionStorage.getItem('token')){
//       console.log('window.location.href='+window.location.href);
//       let Base64 = require('js-base64').Base64;
//       let locals = window.location.href.toString();
//       let local = Base64.encode(locals);
//       console.log('local2='+local)

//       console.log("进入token为空判断, token值为" +  sessionStorage.getItem('token'))
//       // location.href ='https://open.weixin.qq.com/connect/oauth2/authorize?appid=wwb9164107d1885dd1&agentid=1000399&redirect_uri=http://211.136.107.159:8081/lxm-phone/newWxAuth?callback=http://211.136.107.159:8081/lxm_app/m_index.html&response_type=code&scope=SCOPE&state=STATE&connect_redirect=1#wechat_redirect';
//       location.href ='https://open.weixin.qq.com/connect/oauth2/authorize?appid=wwb9164107d1885dd1&agentid=1000399&redirect_uri=http://211.136.107.159:8081/lxm-phone/newWxAuth?callback='+ local +'&response_type=code&scope=SCOPE&state=STATE&connect_redirect=1#wechat_redirect';
//     }else{
//       console.log('进来了')
//       next()
//     }
//   }else{
//     var reg = RegExp(/token/);
//     let str = window.location.href;
//     if(str && str.match(reg)){
//         let index = str.indexOf('token=')
//         let arr = str.substring(index+6,str.length)
//         console.log('主页token='+arr)
//         sessionStorage.setItem('token',arr)
//     }
//     next()
//   }
// });

export default router
