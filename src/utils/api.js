import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://211.136.107.159:8081/lxm-phone/', // api的base_url
  // baseURL: 'http://12.120.100.117:8081/lxm-phone/', // api的base_url
  timeout: 35000, // 请求超时时间
  // headers: { 'content-type': 'application/json;charset=UTF-8'},
})



// 拦截请求
instance.interceptors.request.use(
  config => {
    // if(sessionStorage.getItem('token')){
    config.headers['Authorization'] = 'Idcard eyJhbGciOiJIUzUxMiJ9.eyJyYW5kb21LZXkiOiJpMWZ0ZWwiLCJzdWIiOiJ7XCJyb2xlXCI6XCJjb21tb25cIixcIm5hbWVcIjpcIua1i-ivlWFcIixcInVzZXJpZFwiOlwiMTU4OTk4NjQ1MjZcIn0iLCJleHAiOjE2MzYwOTA3NzAsImlhdCI6MTYzNTQ4NTk3MH0.o0S2qb8b-n6omKSzQseCOE_18AXJ9RuShNsVoE7BACZqzcnTy5bcaPNFEEtDlVlotLfrZ3aFkZW6HuqB-hEBPg';
    // config.headers['Authorization'] = 'Idcard '+ sessionStorage.getItem('token')
    return config
  }, error => {
    return Promise.reject(error)
  }
)

// 拦截响应
instance.interceptors.response.use(
  res => {
    if(res.status !== 200 ){
      return Promise.resolve(res.msg || 'error')
    }else{
      return res.data;
    }
    // 可以在此处拦截接口错误
    // return res
  }, error => {
    return Promise.reject(error)
  }
)

// const instancePhone = axios.create({
//   baseURL: 'http://211.136.107.159:8081/', // api的base_url
//   timeout: 35000, // 请求超时时间
// })

// instancePhone.interceptors.request.use(config => {
//   //  可以在此处添加 token
// console.log(config,'config')
// return config
// }, error => {
// return Promise.reject(error)
// })
// // 拦截响应
// instancePhone.interceptors.response.use(res => {
//   console.log(res,'拦截相应')
// // 可以在此处拦截接口错误
// return res
// }, error => {
// return Promise.reject(error)
// })



// //------手机端
// //获取手机端 jwt Token
// export function getJwt(param) {
//   return instancePhone.get('lxm-phone/wxAuth?'+param)
// }

// // 登录
// export function login (param) {
//   return service.post('user/login' + param)
// }


//------公用接口
//上传图片
export function uploadPic(param) {
  return instance.post('common/upload', param)
}
//删除图片
export function removePic(param) {
  return instance.post('common/remove/file' + param)
}
//发送消息卡片
export function sendCardMsg(param) {
  return instance.post('event/sendCardMsg',param)
}
//AI事件详情
export function aiLoad(param) {
  return instance.post('event/ai/load',param)
}
//人工上报事件详情
export function personLoad(param) {
  return instance.post('event/person/load',param)
}

//------监管力量
//删除用户接口
export function powerDelete(param) {
  return instance.post('manageUser/delete', param)
}
//查询用户及权限
export function powerGetManageUser(param) {
  return instance.post('manageUser/getManageUser' , param)
}
//更新权限接口
export function powerPermission(param) {
  return instance.post('manageUser/update/permission' , param)
}
//更新用户信息接口
export function powerInfo(param) {
  return instance.post('manageUser/update/info', param)
}
//新增用户信息接口
export function powerAdd(param) {
  return instance.post('manageUser/add',param)
}
//获取微信数据
export function powerGetWeiXin(param) {
  return instance.get('manageUser/getWeiXin?userid='+param+'')
}
//查询用户接口
export function powerList(param) {
  return instance.get('manageUser/list')
}
//查询用户列表接口
export function powerUserList(param){
  return instance.post('manageUser/list')
}

//------事件列表
//AI与人工合并接口
export function listPageList(param) {
  return instance.post('event/pageList' , param)
}
//ai事件列表
export function listPageAiList(param) {
  return instance.post('event/ai/pageList',param)
}
// //更新ai事件状态
export function updateStatus(param) {
  return instance.post('event/ai/updateStatus',param)
}
//ai上报市容顽症事件
export function aiReport(param) {
  return instance.post('event/ai/report',param)
}

//--------事件发现
//人工事件上报
export function disInsert(param) {
  return instance.post('event/person/insert' , param)
}
//获取市容顽症总数
export function disGetCount(param) {
  return instance.get('event/getCount')
}

//-------在办事件
//更新人工事件状态（事件结案，事件到人）
export function workUpdate(param) {
  return instance.post('event/person/update',param)
}
//事件到人-填写责任人信息
export function workToUser(param) {
  return instance.post('event/person/eventToDutyUser',param)
}
//垃圾统计
export function workGarStatistical(param){
  return instance.post('event/getGarbageStatistics')
}


//垃圾清运数量上报
export function garbageInsert(param) {
  return instance.post('garbage/count/insert',param)
}
