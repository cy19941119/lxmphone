
import Vue from 'vue'

import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    left:[],
    right:[],
    nav:[],
    user_token:'',
    code:'',
    wxToken:''
  },
  getters: {
      setname(state){
        return state.name+1
      }
  },
  mutations: {
    changename(state,n){
        state.name = 'aaaa'
    },
    changename1(state,n){
      state.name = n
    }
  },
  actions: {
    changename(context,n){
      setTimeout(function(){
        context.commit('changename',n)
      },2000)

    }
  }
})
export default store
