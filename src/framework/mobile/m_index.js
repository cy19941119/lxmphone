// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'
import App from './App'
import Vant from 'vant'
import 'vant/lib/index.css'
import echarts from 'echarts'
import store from '../../store/index'
import router from '../../router/mobile/index'
import _ from 'loadsh'
import Vconsole from 'vconsole'
const vconsole = new Vconsole()
Vue.use(vconsole);
Vue.use(Vant)
const Base64 = require('js-base64').Base64;
import VueScroller from 'vue-scroller'
Vue.use(VueScroller)
Vue.prototype.$echarts = echarts
Vue.prototype._ = _
Vue.config.productionTip = false
/* eslint-disable no-new */



new Vue({
  el: '#app',
  router,
  store,
  Base64,
  components: { App },
  template: '<App/>'
})

